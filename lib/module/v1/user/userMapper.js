/**
 * This file will have request and response object mappings.
 */
import _ from 'lodash';
import contstants from '../../../constant.js';

const loginMapping = (params, accessToken) => {
  const respObj = {
    name: params.name,
    email: params.email,
    username: params.username,
  };

  if (params.gender) {
    respObj.gender = params.gender;
  }
  if (params.dob) {
    respObj.dob = params.dob;
  }
  if (params.designation) {
    respObj.designation = params.designation;
  }
  if (params.companyName) {
    respObj.companyName = params.companyName;
  }
  if (params.profileImage) {
    respObj.profileImage = params.profileImage;
  }
  if (params.phoneNo) {
    respObj.phoneNo = params.phoneNo;
  }
  if (params.deviceToken) {
    respObj.deviceToken = params.deviceToken;
  }
  if (params.deviceID) {
    respObj.deviceID = params.deviceID;
  }
  if (params.deviceTypeId) {
    respObj.deviceTypeId = params.deviceTypeId;
  }
  if (params.profileImage) {
    respObj.profileImage = params.profileImage;
  }

  return {
    message: contstants.MESSAGES.USER_LOGIN,
    accessToken,
    result: respObj,
  };
};

const createMapping = (params) => {
  const respObj = {
    name: params.name,
    email: params.email,
    username: params.username,
  };

  if (params.gender) {
    respObj.gender = params.gender;
  }
  if (params.dob) {
    respObj.dob = params.dob;
  }
  if (params.designation) {
    respObj.designation = params.designation;
  }
  if (params.companyName) {
    respObj.companyName = params.companyName;
  }
  if (params.profileImage) {
    respObj.profileImage = params.profileImage;
  }
  if (params.phoneNo) {
    respObj.phoneNo = params.phoneNo;
  }
  if (params.deviceToken) {
    respObj.deviceToken = params.deviceToken;
  }
  if (params.deviceID) {
    respObj.deviceID = params.deviceID;
  }
  if (params.deviceTypeId) {
    respObj.deviceTypeId = params.deviceTypeId;
  }

  return {
    message: contstants.MESSAGES.OTP_SEND,
    result: respObj,
  };
};

const otpResend = (email) => ({
  message: contstants.MESSAGES.OTP_SEND,
  email,
});

const changePassword = (params) => ({
  message: contstants.MESSAGES.PASSWORD_CHANGED,
  email: params.email,
});

const logout = () => ({
  message: contstants.MESSAGES.LOGOUT,
});

const deleteAccount = () => ({
  message: contstants.MESSAGES.ACCOUNT_DELETED,
});

const socialSignup = (params) => ({
  msg: contstants.MESSAGES.USER_LOGIN,
});

export default {
  loginMapping,
  otpResend,
  logout,
  deleteAccount,
  createMapping,
  changePassword,
  socialSignup,
};

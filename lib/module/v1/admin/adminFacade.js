//= ========================= Load Modules Start =======================

//= ========================= Load internal modules ====================

import adminService from './adminService.js';
import adminMapper from './adminMapper.js';

import appUtils from '../../../appUtils.js';
import redisSession from '../../../redisClient/session.js';
import customException from '../../../customException.js';
import redis from '../../../redisClient/init.js';

//= ========================= Load Modules End ==============================================

const create = async(params) => {
  try {
    const createAdmin = await adminService.createAdmin(params);

    if (createAdmin === 0) {
      throw customException.alreadyRegistered();
    }

    return adminMapper.createMapping(createAdmin);
  } catch (err) {
    // throw error if error occurs
    return err;
  }
};

const adminLogin = async(params) => {
  try {
    const adminDetails = await adminService.isEmailExist(params.email);

    if (!adminDetails) {
      throw customException.wrongCredentials();
    }

    if (adminDetails && adminDetails.status == 2) {
      throw customException.adminNotFound();
    }

    if (!(appUtils.compareHashPassword(params.password, adminDetails.password))) {
      throw customException.wrongCredentials();
    }

    const accessTokenObj = {
      ip: params.clientIp,
      userId: adminDetails._id,
      adminObj: _buildadminTokenGenObj(adminDetails),
    };

    const { token } = await redisSession.create(accessTokenObj);

    return adminMapper.loginMapping(adminDetails, token);
  } catch (err) {
    return err;
  }
};

const resetPassword = async(params) => {
  try {
    const admin = await adminService.changePassword(params);

    return adminMapper.changePassword(admin);
  } catch (err) {
    return err;
  }
};

const logout = () => adminMapper.logout();

const deleteAccount = async(email) => {
  try {
    await adminService.deleteAccount(email);

    return adminMapper.deleteAccount();
  } catch (err) {
    return err;
  }
};

const _buildadminTokenGenObj = (admin) => {
  const role = 'Admin';

  const adminObj = {};
  adminObj.role = role;
  adminObj.adminId = admin._id.toString();
  adminObj.adminType = admin.adminType;
  adminObj.status = admin.status;
  return adminObj;
};

//= ========================= Export Module Start ==============================

export default {
  adminLogin,
  create,
  resetPassword,
  logout,
  deleteAccount,
};

//= ========================= Export Module End ================================

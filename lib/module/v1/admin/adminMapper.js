/**
 * This file will have request and response object mappings.
 */
import _ from 'lodash';
import contstants from '../../../constant.js';

const createMapping = (params) => {
  const respObj = {
    email: params.email,
    adminType: params.adminType,
  };

  return {
    message: contstants.MESSAGES.ADMIN_CREATED,
    result: respObj,
  };
};

const loginMapping = (params, token) => ({
  message: contstants.MESSAGES.USER_LOGIN,
  token,
  email: params.email,
});

const changePassword = (params) => ({
  message: contstants.MESSAGES.PASSWORD_CHANGED,
  email: params.email,
});

const logout = () => ({
  message: contstants.MESSAGES.LOGOUT,
});

const deleteAccount = () => ({
  message: contstants.MESSAGES.ACCOUNT_DELETED,
});

export default {
  logout,
  loginMapping,
  deleteAccount,
  createMapping,
  changePassword,
};

import express from 'express';

import userRoute from './user/userRoute.js';
import adminRouter from './admin/adminRoute.js';

const router = express.Router();

//= ========================= Export Module Start ==========================

// API version 1
router.use('/user', userRoute);
router.use('/admin', adminRouter);

export default router;
//= ========================= Export Module End ============================

import constant from './constant.js';
import customException from './customException.js';
import APIResponse from './model/APIResponse.js';

import sendgrid_email from './service/sendgrid_email.js';
import config from './config/index.js';
import loogerh from './logger/index.js';

const { logger } = loogerh;

const _sendResponse = (response, result) =>
// send status code 200
  response.send(result);

const sendError = (response, error, request) => {
  // if error doesn't has sc than it is an unhandled error,
  // log error, and throw intrnl server error
  if (!error.errorCode) {
    logger.error(error, 'Unhandled error.');
    error = customException.intrnlSrvrErr(error);
  }
  const result = new APIResponse(constant.STATUS_CODE.ERROR, error, request);
  _sendResponse(response, result);
};

const handleError = (error, request, response, next) => {
  // unhandled error
  sendError(response, error, request);
};

const sendSuccess = (response, result, request) => {
  result = new APIResponse(constant.STATUS_CODE.SUCCESS, result, request);
  _sendResponse(response, result);
};

const sendErrorEmail = (params) => {
  // console.log("sendErrorEmail")
  const {
    env, response, error, request, headers, url,
  } = params;
  const payload = {
    email: config.cfg.smtp.errorLogEmail,
    subject: 'Demo : Server Error',
    template: 'errorLog.ejs',
    env,
    response,
    error,
    request,
    headers,
    url,
  };
  sendgrid_email.sendEmail(payload);
};

// ========================== Export Module Start ==========================
export default {
  sendError,
  handleError,
  sendSuccess,
  sendErrorEmail,
};
// ========================== Export Module End ============================

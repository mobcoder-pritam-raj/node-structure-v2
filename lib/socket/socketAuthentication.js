//= ========================= Load internal Module =========================

import redisSession from '../redisClient/session.js';
import customException from '../customException.js';

import userService from '../module/v1/user/userService.js';

//= ========================= Load Modules End =============================

const __verifyTok = async(acsTokn) => {
  try {
    return await redisSession.getByToken(acsTokn);
  } catch (err) {
    next(err);
  }
};

const authSocketTkn = (socket, next) => {
  const { accessToken } = socket.handshake.query;

  if (!accessToken) {
    return next(customException.unauthorizeAccess());
  }

  next();
  __verifyTok(accessToken)
    .bind({})
    .then((tokenPayload) => {
      if (tokenPayload.d) {
        const paylod = tokenPayload.d;
        socket.payload = tokenPayload.d;

        return userService.getByKey({ _id: paylod.userId });
      }
      next();
    })
    .then((user) => {
      socket.user = user;
      next();
    })
    .catch((err) => {
      next(new Error('Authentication error'));
    });
};

export default {
  authSocketTkn,
};

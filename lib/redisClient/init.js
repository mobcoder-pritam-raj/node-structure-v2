import { createClient } from 'redis';
import config from '../config/index.js';

// creating the redisClient
const client = createClient(config.cfg.redis.port, config.cfg.redis.server);
client.on('error', (err) => console.log('Redis Client Error', err));
await client.connect();

// set the key and value
const setValue = async(key, value) => {
  await client.set(key, value, { EX: config.cfg.redis.exp }, (err, reply) => {
    if (err) throw err;
    return reply;
  });
};

// get the key and the value
const getValue = async(key) => await client.get(key, (err, reply) => {
  if (err) throw err;
  return reply;
});

// expire time update
const expire = async(key, exp) => {
  await client.expire(key, exp, (err, reply) => {
    if (err) throw err;
    return reply;
  });
};

// delete the key and value
const deleteValue = async(key) => {
  await client.del(key, (err, reply) => {
    if (err) throw err;
    return reply;
  });
};

export default {
  deleteValue,
  expire,
  getValue,
  setValue,
};

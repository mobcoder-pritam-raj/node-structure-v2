import ejs from 'ejs';
import sgMail from '@sendgrid/mail';
import config from '../config/index.js';

sgMail.setApiKey(config.cfg.sendgridKey);
const appName = config.cfg.app.app_name;

const _templateRead = (async(template, params) => {
  params.appName = appName;
  const filename = `lib/emailTemplate/${template}`;

  return await ejs.renderFile(filename, params)
    .then((result) => result)
    .catch((err) => {
      console.log(err);
      throw err;
    });
});

const sendEmail = (async(payload) => {
  const htmlContent = await _templateRead(payload.template, payload);

  const msg = {
    to: payload.email,
    from: config.cfg.smtp.fromEmail,
    subject: payload.subject,
    html: htmlContent,
  };
  if (payload.cc) {
    msg.cc = payload.cc;
  }
  if (payload.bcc) {
    msg.bcc = payload.bcc;
  }

  return await sgMail.send(msg)
    .then((result) => {
      console.log('Email sent sucessfully!');
      return result;
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
});

// ========================== Export Module Start ==========================
export default {
  sendEmail,
};
// ========================== Export Module End ============================

import nodemailer from 'nodemailer';
import ejs from 'ejs';
import config from '../config/index.js';

const appName = config.cfg.app.app_name;

const _templateRead = ((template, param) => {
  const filename = `lib/emailTemplate/${template}`;
  let data;

  ejs.renderFile(filename, param, (err, htmlData) => {
    if (htmlData) {
      data = htmlData;
    } else {
      console.log(err);
      throw err;
    }
  });

  return data;
});

const sendEmail = ((payload) => {
  payload.appName = appName;
  const { fromEmail } = config.cfg.smtp;
  const toEmail = payload.email;
  const { subject } = payload;
  const content = _templateRead(payload.template, payload);

  const smtpTransport = nodemailer.createTransport({
    service: config.cfg.smtp.fromEmail,
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: config.cfg.smtp.auth.user,
      pass: config.cfg.smtp.auth.pass,
    },
  });

  // setup email data with unicode symbols
  const mailOptions = {
    from: fromEmail, // sender address.  Must be the same as authenticated user if using Gmail.
    to: toEmail, // receiver
    subject, // subject
    html: content, // html body
  };

  return new Promise((resolve, reject) => {
    smtpTransport.sendMail({ mailOptions }, (err, data) => {
      if (err) {
        return reject(err);
      }
      resolve(data);
    });
  });
});

// ========================== Export Module Start ==========================
export default {
  sendEmail,
};
// ========================== Export Module End ============================

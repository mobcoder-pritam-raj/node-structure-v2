//= ========================= Load Modules Start ===========================

//= ========================= Load External Module =========================

import sha256 from 'sha256';
import bcrypt from 'bcrypt';
import randomstring from 'randomstring';
//= ========================= Load Modules End =============================

//= ========================= Export Module Start ===========================
const saltRounds = 10;

// hash a password
const encryptHashPassword = (password) => {
  const salt = bcrypt.genSaltSync(saltRounds);
  return bcrypt.hashSync(password, salt);
};

// compare a hash passsword
const compareHashPassword = (passsword, hashPassword) => bcrypt.compareSync(passsword, hashPassword);

const isValidEmail = ((email) => {
  const pattern = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
  return new RegExp(pattern).test(email);
});

const isValidZipCode = ((zipcode) => {
  const pattern = /^\d{5}(-\d{4})?$/;
  return new RegExp(pattern).test(zipcode);
});

const createHashSHA256 = ((pass) => sha256(pass));

const getRandomPassword = () => getSHA256(Math.floor((Math.random() * 1000000000000) + 1));

const getSHA256 = (val) => sha256(`${val}password`);

const stringToBoolean = (string) => {
  switch (string.toLowerCase().trim()) {
  case 'true':
  case 'yes':
  case '1':
    return true;
  case 'false':
  case 'no':
  case '0':
  case null:
    return false;
  default:
    return Boolean(string);
  }
};

const getRandomOtp = () =>
// Generate Random Number
  randomstring.generate({
    charset: 'numeric',
    length: 6,
  });

// valid mobile ----("+91 1234567890")
const isValidPhone = (mobileNumber) => {
  const regex = /^(?:(?:\+|)91)?[ -]?(?:\d[ -]?\d{2}[ -]?\d{5}|\d{3}[ -]?\d{4}[ -]?\d{3})$/;
  return regex.test(mobileNumber);
};

//= ========================= Export Module Start ===========================

export default {
  isValidEmail,
  isValidZipCode,
  createHashSHA256,
  getRandomPassword,
  encryptHashPassword,
  compareHashPassword,
  stringToBoolean,
  getRandomOtp,
  isValidPhone,
};

//= ========================= Export Module End===========================

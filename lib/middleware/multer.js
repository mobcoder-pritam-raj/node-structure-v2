import multer from 'multer';
import fs from 'fs';
import config from '../config/index.js';

// Set up Multer storage configuration
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/'); // File will be saved in the "uploads" directory
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname); // Use the original file name as the file name
  },
});

// Set up Multer upload middleware with the storage configuration
const upload = multer({ storage });

const single = ((file) => upload.single(file));

const multiple = ((files, size) => upload.array(files, size));
// ========================== Export Module Start ==========================
export default {
  single,
  multiple,
};
// ========================== Export Module End ============================

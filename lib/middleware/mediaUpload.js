import fs from 'fs';
import _ from 'lodash';

import { S3Client, PutObjectCommand } from '@aws-sdk/client-s3';
import loggerh from '../logger/index.js';

// import AWS from 'aws-sdk'

import config from '../config/index.js';

const { logger } = loggerh;

const s3 = new S3Client({
  region: config.cfg.s3.region,
  credentials: {
    accessKeyId: config.cfg.iamUser.keyId,
    secretAccessKey: config.cfg.iamUser.accessKey,
  },
  signatureVersion: config.cfg.s3.signatureVersion,
});

const Bucket = config.cfg.s3.bucketName;

const _fetchFilesFromReq = ((request) => {
  if (request.file) {
    return [request.file];
  }
  if (request.files) {
    return request.files;
  }

  // No Data
});

const __deleteFiles = ((filePathList) => {
  try {
    filePathList.map((file) => {
      fs.unlinkSync(file);
    });
  } catch (err) {
    logger.error(err);
  }
});

const uploadSingleMediaToS3 = (() => (request, response, next) => {
  const files = _fetchFilesFromReq(request);
  if (!files) {
    return next();
  }

  const file = files[0];
  const params = {
    Bucket,
    Key: String(file.filename),
    Body: fs.createReadStream(file.path),

  };

  const command = new PutObjectCommand(params);

  return new Promise((resolve, reject) => {
    s3.send(command, (error, data) => {
      if (data) {
        const locationUrl = `https://${params.Bucket}.s3.amazonaws.com/${params.Key}`;

        request.body.location = locationUrl;
        resolve;
      } else {
        logger.error(error);
        reject;
      }

      __deleteFiles(_.map(files, 'path'));

      return next();
    });
  });
});

const uploadMultipleMediaToS3 = (() => ((request, response, next) => {
  const files = _fetchFilesFromReq(request);
  if (!files) {
    return next();
  }

  const location = [];
  files.map((files) => {
    const file = [];
    file.push(files);
    const params = {
      Bucket,
      Key: String(file.filename),
      Body: fs.createReadStream(file.path),

    };

    const command = new PutObjectCommand(params);

    return new Promise((resolve, reject) => {
      s3.send(command, (error, data) => {
        if (data) {
          const locationUrl = `https://${params.Bucket}.s3.amazonaws.com/${params.Key}`;

          location.push(locationUrl);
          resolve;
        } else {
          logger.error(error);
          reject;
        }

        __deleteFiles(_.map(files, 'path'));
      });
      request.body.location = location;
      return next();
    });
  });
}));

// ========================== Export Module Start ==========================
export default {
  uploadSingleMediaToS3,
  uploadMultipleMediaToS3,
};
// ========================== Export Module End ============================

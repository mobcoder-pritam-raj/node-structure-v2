//= ========================= Load Modules Start ===========================

//= ========================= Load Internal Module =========================

// Load exceptions
import Exception from './model/Exception.js';
import constants from './constant.js';
//= ========================= Load Modules End =============================

//= ========================= Export Module Start ===========================

export default {
  intrnlSrvrErr: (err) => new Exception(1, constants.MESSAGES.INTERNAL_SERVER_ERROR, err),
  unauthorizeAccess: (err) => new Exception(2, constants.MESSAGES.UNAUTHORIZED_ACCESS, err),
  alreadyRegistered: (err) => new Exception(3, constants.MESSAGES.ALREADY_EXIST, err),
  invalidEmail: () => new Exception(4, constants.MESSAGES.INVALID_EMAIL),
  getCustomErrorException: (errMsg, error) => new Exception(5, errMsg, error),
  userNotFound: () => new Exception(3, constants.MESSAGES.USER_NOT_REGISTERED),
  wrongCredentials: () => new Exception(3, constants.MESSAGES.INCORRECT_PASS),
  otpNotValid: () => new Exception(3, constants.MESSAGES.WRONG_OTP),
  emailNotVerified: () => new Exception(3, constants.MESSAGES.EMAIL_NOT_VERIFIED),
  adminNotFound: () => new Exception(3, constants.MESSAGES.ADMIN_NOT_EXIST),
};

//= ========================= Export Module   End ===========================

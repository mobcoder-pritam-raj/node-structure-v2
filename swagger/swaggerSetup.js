import swaggerUi from 'swagger-ui-express';
import { SwaggerTheme } from 'swagger-themes';
import swaggerDocument from './swagger.js';

const theme = new SwaggerTheme('v3');

const options = {
  explorer: true,
  customCss: theme.getBuffer('dark'),
};

const swagger = ((app) => {
  app.use('/apiDocs/v1', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));
});

export default swagger;
